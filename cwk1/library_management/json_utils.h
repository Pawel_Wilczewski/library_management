#ifndef COMP1921_JSON_UTILS_H
#define COMP1921_JSON_UTILS_H

#include "book_manager.h"
#include "user_manager.h"

// constants below are used when dumping structs/arrays/whatever else to json

const char *JSON_ARRAY_PREFIX;
const char *JSON_ARRAY_SUFFIX;

const char *JSON_STRUCT_PREFIX;
const char *JSON_STRUCT_SUFFIX;

const char *JSON_INDENT;
const char *JSON_DELIMITER;
const char *JSON_WHITESPACE;
const char *JSON_ASSIGNMENT;
const char *JSON_QUOTE;

// returns appropriate prefix that will be applied to each line of json string
char *get_indent_prefix(unsigned int indent);

// returns string without any JSON_INDENT and JSON_WHITESPACE
char *get_without_whitespaces(const char *string);

// returns string like JSON_QUOTE + name + JSON_QUOTE + JSON_ASSIGNMENT
char *get_json_assignment(const char *name);

// returns string value from json variable (right side of assignment without quotation marks)
char *get_json_value(const char *json_var);

// converts string variable to row usable within json file
char *string_to_json(const char *name, const char *value);

// converts int variable to row usable in json string
char *int_to_json(const char *name, int value);

// returns first line of array converted to json
char *arr_to_json(const char *name);

// converts arguments to string in a standardized fashion (allows for easy conversion struct->string)
// format is like: "%s varname1 %i varname2 %c varname3" and so on
char *vars_to_json(unsigned int indent, const char *format, ...);

// converts a book to its equivalent json string
char *Book_to_json(unsigned int indent, const Book *book);

// converts a book array to its equivalent json string
char *BookArray_to_json(unsigned int indent, const BookArray *books);

// converts books owned to its equivalent json string
char *BooksOwned_to_json(unsigned int indent, const BooksOwned *books);

// converts a books owned array to its equivalent json string
char *BooksOwnedArray_to_json(unsigned int indent, const BooksOwnedArray *books);

// converts user to its equivalent json string
char *User_to_json(unsigned int indent, const User *user);

// converts a user array to its equivalent json string
char *UserArray_to_json(unsigned int indent, const UserArray *users);

// converts json equivalent of book to book
Book *json_to_Book(const char *string);

// converts json equivalent of book array to book array
BookArray *json_to_BookArray(const char *string);

// converts json equivalent of books owned to books owned
BooksOwned *json_to_BooksOwned(const char *string);

// converts json equivalent of books owned array to books owned array
BooksOwnedArray *json_to_BooksOwnedArray(const char *string);

// converts json equivalent of user to user
User *json_to_User(const char *string);

// converts json equivalent of user array to user array
UserArray *json_to_UserArray(const char *string);

#endif //COMP1921_JSON_UTILS_H
