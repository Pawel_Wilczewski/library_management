#ifndef COMP1921_STRING_UTILS_H
#define COMP1921_STRING_UTILS_H

#include <stdarg.h>
#include <stdio.h>

// handy string array, used i. e. in str_split
typedef struct {
    char **array;
    size_t length;
} StringArray;

// returns 1 if strings are identical, else returns 0 (for safety ensures '\0' at the end of both of the strings)
int str_same(const char *str1, const char *str2);

// returns 0 if string is invalid or like "", else returns the length of the given string argument
size_t str_length(const char *string);

// find index of first occurrence of pattern pattern in string
int str_find(const char *string, const char *pattern, size_t start_index);

// find index of last occurrence of pattern pattern in string, stop_index: use < 0 for strlen
int str_find_r(const char *string, const char *pattern, int stop_index);

// returns the amount of occurrences of a substring in string
int str_count(const char *string, const char *substring);

// returns 1 if string contains the substring, 0 otherwise
int str_contains(const char *string, const char *substring);

// returns a pointer to the duplicate of the given string
char *str_copy(const char *string);

// returns str1 + str2 (concatenation, safer than strcat)
char * str_combine(const char * str1, const char * str2);

// returns lowercase string
char *str_lower(const char *string);

// returns uppercase string
char *str_upper(const char *string);

// returns reverse of the given string argument
char *str_reverse(const char *string);

// replaces first n occurrences of a substring in string (<1 for all)
char *str_replace(char *string, const char *to_replace, const char *replace_with, int n);

// replaces first n occurrences of a substring in string (<1 for all) from the right
char *str_replace_r(char *string, const char *to_replace, const char *replace_with, int n);

// returns a string that has replaced content between the two patterns (including pattern1 if include1 == 0 etc.)
// if pattern2 not found after pattern1, or pattern2 is null, replaces everything after pattern1
// n: how many times to replace, <= 0 to replace all
// WARNING: doesnt support nesting (like [[x + 2 + [x + 1]] + 3])
char *str_replace_between(char *string, const char *pattern1, const char *pattern2, const char *replace_with,
                          int include1, int include2, int n);

// start - inclusive, stop - exclusive, if stop < start: only account for start
char *str_substr(const char *string, size_t start, size_t stop);

// excluding the pattern
char *str_substr_after(const char *string, const char *pattern, int inclusive);

// excluding the pattern
char *str_substr_before(const char *string, const char *pattern, int inclusive);

// returns an array of strings split by delimiter
StringArray *str_split(const char *string, const char *delimiter);

// safely and cleanly returns string from int
char * int_to_str(int x);

// NOT SAFE, use only when sure the string does contain only int
int str_to_int(const char *string);

#endif //COMP1921_STRING_UTILS_H
