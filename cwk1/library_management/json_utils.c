#include "json_utils.h"
#include <stdlib.h>
#include "book_management.h"
#include "string_utils.h"

const char *JSON_ARRAY_PREFIX = "[";
const char *JSON_ARRAY_SUFFIX = "]";

const char *JSON_STRUCT_PREFIX = "{";
const char *JSON_STRUCT_SUFFIX = "}";

const char *JSON_INDENT = "    ";
const char *JSON_DELIMITER = ",";
const char *JSON_WHITESPACE = "\n";
const char *JSON_ASSIGNMENT = ": ";
const char *JSON_QUOTE = "\"";

char *get_indent_prefix(const unsigned int indent)
{
    // returns JSON_INDENT * indent
    char *indent_prefix = "";
    for (int i = 0; i < indent; ++i)
        indent_prefix = str_combine(indent_prefix, JSON_INDENT);

    return indent_prefix;
}

char *get_without_whitespaces(const char *string)
{
    // removes whitespaces from the given json string
    char *out = str_copy(string);
    out = str_replace(out, JSON_INDENT, "", -1);
    out = str_replace(out, JSON_WHITESPACE, "", -1);
    return out;
}

char *get_json_assignment(const char *name)
{
    return str_combine(str_combine(str_combine(JSON_QUOTE, name), JSON_QUOTE), JSON_ASSIGNMENT);
}

char *get_json_value(const char *json_var)
{
    // strip the var until it only contains the desired content (value after the assignment without quotation marks)
    char *out = str_copy(json_var);
    out = str_replace(out, str_substr_before(out, JSON_ASSIGNMENT, 1), "", 1);
    out = str_replace(out, JSON_QUOTE, "", 1);
    out = str_replace_r(out, JSON_QUOTE, "", 1);
    return out;
}

char *string_to_json(const char *name, const char *value)
{
    char *out = str_copy(get_json_assignment(name));
    out = str_combine(out, JSON_QUOTE);
    out = str_combine(out, value);
    out = str_combine(out, JSON_QUOTE);
    return out;
}

char *int_to_json(const char *name, int value)
{
    char *out = str_copy(get_json_assignment(name));
    out = str_combine(out, int_to_str(value));
    return out;
}

char *arr_to_json(const char *name)
{
    char *out = str_copy(get_json_assignment(name));
    out = str_combine(out, JSON_ARRAY_PREFIX);
    return out;
}

char *vars_to_json(unsigned int indent, const char *format, ...)
{
    va_list vl;
    va_start(vl, format);

    // prepare the appropriate prefix
    char *indent_prefix = get_indent_prefix(indent);

    char *todo = str_copy(format);
    char *out = str_copy(JSON_STRUCT_PREFIX);
    out = str_combine(out, JSON_WHITESPACE);

    int i = 0;
    while (str_contains(todo, "%"))
    {
        if (i > 0)
        {
            out = str_combine(out, JSON_DELIMITER);
            out = str_combine(out, JSON_WHITESPACE);
        }

        // get the currently evaluated part (if last % - get all the remaining part of the string)
        char *doing = str_count(todo, "%") > 1 ? str_substr_before(str_substr(todo, 1, -1), "%", 0)
                                               : str_substr_after(todo, "%", 0);

        // update the remaining part for the next iteration
        todo = str_replace(todo, str_combine("%", doing), "", 1);

        // get hold of the variable name and remove white spaces from it
        char *var_name = str_substr(doing, 1, -1);
        var_name = str_replace(var_name, " ", "", -1);
        var_name = str_replace(var_name, "\t", "", -1);
        var_name = str_replace(var_name, "\n", "", -1);

        out = str_combine(out, indent_prefix);
        out = str_combine(out, JSON_INDENT);

        switch (doing[0])
        {
            case 'c':
            {
                // in case of working with char
                char c = (char) va_arg(vl, int);
                char *c_terminated = (char *) malloc(sizeof(char) * 2);
                c_terminated[0] = c;
                c_terminated[1] = '\0';
                out = str_combine(out, string_to_json(var_name, c_terminated));
                break;
            }
            case 's':
            {
                // in case of working with string
                char *str = va_arg(vl, char *);
                out = str_combine(out, string_to_json(var_name, str));
                break;
            }
            case 'i':
            {
                // in case of working with int
                int integer = va_arg(vl, int);
                out = str_combine(out, int_to_json(var_name, integer));
                break;
            }
            default:
            {
                printf("Unsupported type when converting vars to string: %s!", &doing[0]);
                return NULL;
            }
        }
        i++;
    }
    va_end(vl);

    // add the struct suffix
    out = str_combine(out, JSON_WHITESPACE);
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_STRUCT_SUFFIX);

    return out;
}

char *Book_to_json(const unsigned int indent, const Book *book)
{
    if (book == NULL)
        return NULL;

    return vars_to_json(indent, "%s title %s authors %i year %i copies",
                        book->title, book->authors, book->year, book->copies);
}

char *BookArray_to_json(unsigned int indent, const BookArray *books)
{
    if (books == NULL)
        return NULL;

    // create the appropriate prefix
    char *indent_prefix = get_indent_prefix(indent);

    // create line for struct prefix
    char *out = str_copy(JSON_STRUCT_PREFIX);
    out = str_combine(out, JSON_WHITESPACE);

    // create line for length
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_INDENT);
    out = str_combine(out, int_to_json("length", (int) books->length));
    out = str_combine(out, JSON_DELIMITER);

    // create line for array beginning
    out = str_combine(out, JSON_WHITESPACE);
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_INDENT);
    out = str_combine(out, arr_to_json("array"));
    out = str_combine(out, JSON_WHITESPACE);

    // add each book to the output string
    for (int i = 0; i < books->length; ++i)
    {
        if (i > 0)
        {
            out = str_combine(out, JSON_DELIMITER);
            out = str_combine(out, JSON_WHITESPACE);
        }
        out = str_combine(out, indent_prefix);
        out = str_combine(out, JSON_INDENT);
        out = str_combine(out, JSON_INDENT);
        out = str_combine(out, Book_to_json(indent + 2, &books->array[i]));
    }

    // create the suffix of the array
    out = str_combine(out, JSON_WHITESPACE);
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_INDENT);
    out = str_combine(out, JSON_ARRAY_SUFFIX);

    // create the suffix of the entire struct
    out = str_combine(out, JSON_WHITESPACE);
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_STRUCT_SUFFIX);

    return out;
}

char *BooksOwned_to_json(unsigned int indent, const BooksOwned *books)
{
    if (books == NULL)
        return NULL;

    // create the appropriate prefix
    char *indent_prefix = get_indent_prefix(indent);

    // create line for struct prefix
    char *out = str_copy(JSON_STRUCT_PREFIX);
    out = str_combine(out, JSON_WHITESPACE);

    // create line for owner email
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_INDENT);
    out = str_combine(out, string_to_json("owner", books->owner));
    out = str_combine(out, JSON_DELIMITER);

    // create line for book array
    out = str_combine(out, JSON_WHITESPACE);
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_INDENT);

    out = str_combine(out, get_json_assignment("books"));
    out = str_combine(out, BookArray_to_json(indent + 1, books->books));

    // create the suffix of the entire struct
    out = str_combine(out, JSON_WHITESPACE);
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_STRUCT_SUFFIX);

    return out;
}

char *BooksOwnedArray_to_json(unsigned int indent, const BooksOwnedArray *books)
{
    if (books == NULL)
        return NULL;

    // create the appropriate prefix
    char *indent_prefix = get_indent_prefix(indent);

    // create line for struct prefix
    char *out = str_copy(JSON_STRUCT_PREFIX);
    out = str_combine(out, JSON_WHITESPACE);

    // create line for array length
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_INDENT);
    out = str_combine(out, int_to_json("length", (int)books->length));
    out = str_combine(out, JSON_DELIMITER);

    // create line for BooksOwned array
    out = str_combine(out, JSON_WHITESPACE);
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_INDENT);
    out = str_combine(out, arr_to_json("array"));
    out = str_combine(out, JSON_WHITESPACE);

    // add each BooksOwned to the output string
    for (int i = 0; i < books->length; ++i)
    {
        if (i > 0)
        {
            out = str_combine(out, JSON_DELIMITER);
            out = str_combine(out, JSON_WHITESPACE);
        }
        out = str_combine(out, indent_prefix);
        out = str_combine(out, JSON_INDENT);
        out = str_combine(out, JSON_INDENT);
        out = str_combine(out, BooksOwned_to_json(indent + 2, &books->array[i]));
    }

    // create the suffix of the array
    out = str_combine(out, JSON_WHITESPACE);
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_INDENT);
    out = str_combine(out, JSON_ARRAY_SUFFIX);

    // create the suffix of the entire struct
    out = str_combine(out, JSON_WHITESPACE);
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_STRUCT_SUFFIX);

    return out;
}

char *User_to_json(unsigned int indent, const User *user)
{
    if (user == NULL)
        return NULL;

    return vars_to_json(indent, "%i role %s name %s email %s username %s password",
                        user->role, user->name, user->email, user->username, user->password);
}

char *UserArray_to_json(unsigned int indent, const UserArray *users)
{
    if (users == NULL)
        return NULL;

    // create the appropriate prefix
    char *indent_prefix = get_indent_prefix(indent);

    // create line for struct prefix
    char *out = str_copy(JSON_STRUCT_PREFIX);
    out = str_combine(out, JSON_WHITESPACE);

    // create line for length
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_INDENT);
    out = str_combine(out, int_to_json("length", (int) users->length));
    out = str_combine(out, JSON_DELIMITER);

    // create line for array beginning
    out = str_combine(out, JSON_WHITESPACE);
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_INDENT);
    out = str_combine(out, arr_to_json("array"));
    out = str_combine(out, JSON_WHITESPACE);

    // add each book to the output string
    for (int i = 0; i < users->length; ++i)
    {
        if (i > 0)
        {
            out = str_combine(out, JSON_DELIMITER);
            out = str_combine(out, JSON_WHITESPACE);
        }
        out = str_combine(out, indent_prefix);
        out = str_combine(out, JSON_INDENT);
        out = str_combine(out, JSON_INDENT);
        out = str_combine(out, User_to_json(indent + 2, &users->array[i]));
    }

    // create the suffix of the array
    out = str_combine(out, JSON_WHITESPACE);
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_INDENT);
    out = str_combine(out, JSON_ARRAY_SUFFIX);

    // create the suffix of the entire struct
    out = str_combine(out, JSON_WHITESPACE);
    out = str_combine(out, indent_prefix);
    out = str_combine(out, JSON_STRUCT_SUFFIX);

    return out;
}

Book *json_to_Book(const char *string)
{
    if (string == NULL || str_same(string, ""))
        return NULL;

    // remove all unnecessary part of the json string
    char *str = get_without_whitespaces(string);
    str = str_replace(str, JSON_STRUCT_PREFIX, "", 1);
    str = str_replace_r(str, JSON_STRUCT_SUFFIX, "", 1);

    // go through each of the rows and put the value from that row in the appropriate book property
    Book *out = (Book *) malloc(sizeof(Book));
    StringArray *vars = str_split(str, JSON_DELIMITER);
    for (int i = 0; i < vars->length; i++)
    {
        char *current_var = vars->array[i];

        if (str_contains(current_var, get_json_assignment("title")))
            out->title = get_json_value(current_var);

        else if (str_contains(current_var, get_json_assignment("authors")))
            out->authors = get_json_value(current_var);

        else if (str_contains(current_var, get_json_assignment("year")))
            out->year = str_to_int(get_json_value(current_var));

        else if (str_contains(current_var, get_json_assignment("copies")))
            out->copies = str_to_int(get_json_value(current_var));
    }

    return out;
}

BookArray *json_to_BookArray(const char *string)
{
    if (string == NULL || str_same(string, ""))
        return NULL;

    char *str = get_without_whitespaces(string);

    str = str_replace(str, JSON_STRUCT_PREFIX, "", 1);
    str = str_replace_r(str, JSON_STRUCT_SUFFIX, "", 1);

    // initialize the output array
    BookArray *out = (BookArray *) malloc(sizeof(BookArray));

    // length is the first var in the json string
    char *length_var = str_substr_before(str, JSON_DELIMITER, 0);
    if (length_var == NULL) return NULL;
    out->length = str_to_int(get_json_value(length_var));
    out->array = (Book *) malloc(sizeof(Book) * out->length);

    // assign the out->array by converting books one-by-one
    char *array_var = str_substr_after(str, JSON_DELIMITER, 0);
    if (array_var == NULL) return NULL;

    array_var = str_replace(array_var, get_json_assignment("array"), "", 1);
    array_var = str_replace(array_var, JSON_ARRAY_PREFIX, "", 1);
    array_var = str_replace_r(array_var, JSON_ARRAY_SUFFIX, "", 1);

    StringArray *books = str_split(array_var, str_combine(JSON_STRUCT_SUFFIX, JSON_DELIMITER));
    if (books == NULL) return NULL;

    for (int i = 0; i < books->length; ++i)
    {
        // re-add the struct suffix deleted by the split
        if (i < books->length - 1)
            books->array[i] = str_combine(books->array[i], JSON_STRUCT_SUFFIX);

        out->array[i] = *json_to_Book(books->array[i]);
    }

    return out;
}

BooksOwned *json_to_BooksOwned(const char *string)
{
    if (string == NULL || str_same(string, ""))
        return NULL;

    char *str = get_without_whitespaces(string);

    str = str_replace(str, JSON_STRUCT_PREFIX, "", 1);
    str = str_replace_r(str, JSON_STRUCT_SUFFIX, "", 1);

    BooksOwned *out = (BooksOwned *) malloc(sizeof(BooksOwned));

    // owner is the first var in the json string
    char *owner_var = str_substr_before(str, JSON_DELIMITER, 0);
    if (owner_var == NULL) return NULL;
    out->owner = get_json_value(owner_var);

    // next is the array
    char *array_var = str_substr_after(str, JSON_DELIMITER, 0);
    array_var = str_replace(array_var, str_substr_before(array_var, JSON_ASSIGNMENT, 1), "", 1);

    out->books = json_to_BookArray(array_var);

    return out;
}

BooksOwnedArray *json_to_BooksOwnedArray(const char *string)
{
    if (string == NULL || str_same(string, ""))
        return NULL;

    char *str = get_without_whitespaces(string);

    str = str_replace(str, JSON_STRUCT_PREFIX, "", 1);
    str = str_replace_r(str, JSON_STRUCT_SUFFIX, "", 1);

    // initialize the output array
    BooksOwnedArray *out = (BooksOwnedArray *) malloc(sizeof(BooksOwnedArray));

    // length is the first var in the json string
    char *length_var = str_substr_before(str, JSON_DELIMITER, 0);
    if (length_var == NULL) return NULL;
    out->length = str_to_int(get_json_value(length_var));
    out->array = (BooksOwned *) malloc(sizeof(BooksOwned) * out->length);

    // assign the out->array by converting books owned one-by-one
    char *array_var = str_substr_after(str, JSON_DELIMITER, 0);
    if (array_var == NULL) return NULL;

    array_var = str_replace(array_var, get_json_assignment("array"), "", 1);
    array_var = str_replace(array_var, JSON_ARRAY_PREFIX, "", 1);
    array_var = str_replace_r(array_var, JSON_ARRAY_SUFFIX, "", 1);

    // split by double suffix and delimiter
    StringArray *books_owned = str_split(array_var, str_combine(str_combine(JSON_STRUCT_SUFFIX, JSON_STRUCT_SUFFIX), JSON_DELIMITER));
    if (books_owned == NULL) return NULL;

    for (int i = 0; i < books_owned->length; ++i)
    {
        // re-add the double struct suffix deleted by the split
        if (i < books_owned->length - 1)
            books_owned->array[i] = str_combine(str_combine(books_owned->array[i], JSON_STRUCT_SUFFIX), JSON_STRUCT_SUFFIX);

        out->array[i] = *json_to_BooksOwned(books_owned->array[i]);
    }
    return out;
}

User *json_to_User(const char *string)
{
    if (string == NULL || str_same(string, ""))
        return NULL;

    // remove all unnecessary part of the json string
    char *str = get_without_whitespaces(string);
    str = str_replace(str, JSON_STRUCT_PREFIX, "", 1);
    str = str_replace_r(str, JSON_STRUCT_SUFFIX, "", 1);

    // go through each of the rows and put the value from that row in the appropriate user property
    User *out = (User *) malloc(sizeof(User));
    StringArray *vars = str_split(str, JSON_DELIMITER);
    for (int i = 0; i < vars->length; i++)
    {
        char *current_var = vars->array[i];

        if (str_contains(current_var, get_json_assignment("role")))
            out->role = str_to_int(get_json_value(current_var));

        else if (str_contains(current_var, get_json_assignment("name")))
            out->name = get_json_value(current_var);

        else if (str_contains(current_var, get_json_assignment("email")))
            out->email = get_json_value(current_var);

        else if (str_contains(current_var, get_json_assignment("username")))
            out->username = get_json_value(current_var);

        else if (str_contains(current_var, get_json_assignment("password")))
            out->password = get_json_value(current_var);
    }

    return out;
}

UserArray *json_to_UserArray(const char *string)
{
    if (string == NULL || str_same(string, ""))
        return NULL;

    char *str = get_without_whitespaces(string);

    str = str_replace(str, JSON_STRUCT_PREFIX, "", 1);
    str = str_replace_r(str, JSON_STRUCT_SUFFIX, "", 1);

    // initialize the output array
    UserArray *out = (UserArray *) malloc(sizeof(UserArray));

    // length is the first var in the json string
    char *length_var = str_substr_before(str, JSON_DELIMITER, 0);
    if (length_var == NULL) return NULL;
    out->length = str_to_int(get_json_value(length_var));
    out->array = (User *) malloc(sizeof(User) * out->length);

    // assign the out->array by converting books one-by-one
    char *array_var = str_substr_after(str, JSON_DELIMITER, 0);
    if (array_var == NULL) return NULL;

    array_var = str_replace(array_var, get_json_assignment("array"), "", 1);
    array_var = str_replace(array_var, JSON_ARRAY_PREFIX, "", 1);
    array_var = str_replace_r(array_var, JSON_ARRAY_SUFFIX, "", 1);

    StringArray *users = str_split(array_var, str_combine(JSON_STRUCT_SUFFIX, JSON_DELIMITER));
    if (users == NULL) return NULL;

    for (int i = 0; i < users->length; ++i)
    {
        // re-add the struct suffix deleted by the split
        if (i < users->length - 1)
            users->array[i] = str_combine(users->array[i], JSON_STRUCT_SUFFIX);

        out->array[i] = *json_to_User(users->array[i]);
    }

    return out;
}
