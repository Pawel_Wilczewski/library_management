#include <stdio.h>
#include "book_manager.h"
#include "book_management.h"
#include "json_utils.h"
#include "input_utils.h"

// name of the file containing the users database
static const char *USERS_DATABASE = "users.json";

// name of the file containing the books database
static const char *BOOKS_DATABASE = "books.json";

// library needs to use a specific owner name for BooksOwned->owner
// for regular users it will be email, however library needs something else
static const char *LIBRARY_OWNERSHIP_NAME = "library";

int main()
{
    // initialize users and books
    UserArray *users = load_users(USERS_DATABASE);
    BooksOwnedArray *books = load_books_(BOOKS_DATABASE);

    // prompt to create admin if admin account is not yet created
    if (find_user_by_role(users, Role_Admin)->length < 1)
    {
        printf("0 admin accounts detected!\n");
        printf("Please set up an admin account:\n");

        prompt_register_user(users, Role_Admin);
        store_users(users, USERS_DATABASE);
    }

    while (1)
    {
        // main menu
        clear_screen();
        printf("--- Main Menu ---\n");
        printf("1. Log in...\n");
        printf("2. Register...\n");
        printf("3. Exit.\n");
        char choice = get_valid_char("Choice", "123");

        // log in menu
        if (choice == '1')
        {
            printf("\n\n");
            User *logged_user = prompt_log_in_user(users);

            // administrator / user panel
            while (1)
            {
                clear_screen();

                // admins can add and remove books from library, but also borrow and return books like regular users
                if (logged_user->role == Role_Admin)
                {
                    printf("--- Administration Panel ---\n");
                    printf("1. Borrow book...\n");
                    printf("2. Return book...\n");
                    printf("3. Add book to library...\n");
                    printf("4. Remove book from library...\n");
                    printf("5. Exit.\n");
                    choice = get_valid_char("Choice", "12345");

                    if (choice == '1')
                    {
                        printf("\n\n--- Borrowing book ---\n");
                        prompt_borrow_book(books, logged_user->email, LIBRARY_OWNERSHIP_NAME);
                        store_books_(books, BOOKS_DATABASE);
                    }
                    else if (choice == '2')
                    {
                        printf("\n\n--- Returning book ---\n");
                        prompt_return_book(books, logged_user->email, LIBRARY_OWNERSHIP_NAME);
                        store_books_(books, BOOKS_DATABASE);
                    }
                    else if (choice == '3')
                    {
                        printf("\n\n--- Adding book to library ---\n");
                        prompt_add_book(books, LIBRARY_OWNERSHIP_NAME);
                        store_books_(books, BOOKS_DATABASE);
                    }
                    else if (choice == '4')
                    {
                        printf("\n\n--- Removing book from library ---\n");
                        prompt_remove_book(books, LIBRARY_OWNERSHIP_NAME);
                        store_books_(books, BOOKS_DATABASE);
                    }
                    else if (choice == '5')
                        break;
                }
                // users can only return and borrow books
                else if (logged_user->role == Role_User)
                {
                    printf("--- User Panel ---\n");
                    printf("1. Borrow book...\n");
                    printf("2. Return book...\n");
                    printf("3. Exit.\n");
                    choice = get_valid_char("Choice", "123");

                    if (choice == '1')
                    {
                        printf("\n\n--- Borrowing book ---\n");
                        prompt_borrow_book(books, logged_user->email, LIBRARY_OWNERSHIP_NAME);
                        store_books_(books, BOOKS_DATABASE);
                    }
                    else if (choice == '2')
                    {
                        printf("\n\n--- Returning book ---\n");
                        prompt_return_book(books, logged_user->email, LIBRARY_OWNERSHIP_NAME);
                        store_books_(books, BOOKS_DATABASE);
                    }
                    else if (choice == '3')
                        break;
                }
            }
        }
        // registration panel
        else if (choice == '2')
        {
            printf("\n\n--- Registration ---\n");
            prompt_register_user(users, Role_User);
            store_users(users, USERS_DATABASE);
        }
        // exit
        else if (choice == '3')
            break;
    }

    return 0;
}
