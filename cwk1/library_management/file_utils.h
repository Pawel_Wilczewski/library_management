#ifndef COMP1921_FILE_UTILS_H
#define COMP1921_FILE_UTILS_H

#include <stdio.h>

// returns full content of the file under the given filename
// doesn't close the file after reading it
char * read_file_filename(const char *filename);

// returns full content of the file under the given pointer
// doesn't close the file after reading it
char * read_file(FILE *file);

#endif //COMP1921_FILE_UTILS_H
