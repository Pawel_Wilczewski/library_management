#include "book_manager.h"
#include "string_utils.h"
#include "book_management.h"
#include <stdlib.h>

int store_books_(BooksOwnedArray *books, const char *filename)
{
    if (filename == NULL || books == NULL)
        return 1;

    if (str_same(filename, ""))
        return -1;

    // open file, store books and close it afterwards
    // if the file doesn't exist, "w+" ensures it will be created
    edited_books = books;
    FILE *file = fopen(filename, "w+");
    int result = store_books(file);
    fclose(file);

    return result;
}

BooksOwnedArray *load_books_(const char *filename)
{
    if (filename == NULL || str_same(filename, ""))
        return NULL;

    FILE *file = fopen(filename, "r");

    // if the file didnt exist, initialize an empty BooksOwnedArray, store it and then re-open the file
    if (file == NULL)
    {
        BooksOwnedArray *books = (BooksOwnedArray *) malloc(sizeof(BooksOwnedArray));
        books->length = 0;
        books->array = (BooksOwned *) malloc(sizeof(BooksOwned));

        store_books_(books, filename);
        file = fopen(filename, "r");
    }

    // load books and close the file
    load_books(file);
    fclose(file);

    return edited_books;
}

int book_same(const Book *book1, const Book *book2)
{
    if (book1 == NULL && book2 != NULL)
        return 0;

    if (book1 != NULL && book2 == NULL)
        return 0;

    if (book1 == NULL && book2 == NULL)
        return 1;

    if (str_same(book1->title, book2->title) && str_same(book1->authors, book2->authors) && book1->year == book2->year)
        return 1;
    else
        return 0;
}

int add_book_(BooksOwnedArray *books_database, const char *owner,
              char *title, char *authors, unsigned int year, unsigned int copies)
{
    // if no books found under the given owner name, create a new BooksOwned and insert it to the database first
    BooksOwned *books_owned = find_books_by_owner(books_database, owner);
    if (books_owned == NULL)
    {
        books_owned = (BooksOwned *) malloc(sizeof(BooksOwned));
        books_owned->owner = str_copy(owner);
        books_owned->books = (BookArray *) malloc(sizeof(BookArray));
        books_owned->books->length = 0;

        add_books(books_database, books_owned);
    }

    // return error code if any errors occur
    if (books_owned == NULL || title == NULL || authors == NULL)
        return 1;

    if (str_same(title, "") || str_same(authors, ""))
        return -1;

    edited_books_single = books_owned;

    // initialize the book with all the parameters given
    Book *book = (Book *) malloc(sizeof(Book));
    book->title = title;
    book->authors = authors;
    book->year = year;
    book->copies = copies;

    return add_book(*book);
}

int remove_book_(const BooksOwnedArray *books_database, const char *owner, const Book *book, const int copies_to_remove)
{
    // if book is not in the database, return an error code (not really an error though)
    BooksOwned *books = find_books_by_owner(books_database, owner);

    if (book == NULL || books == NULL)
        return 1;

    edited_books_single = books;

    // create a copy of the book with adjusted copies_to_remove and pass it to the remove_book function
    Book *book_copy = (Book *) malloc(sizeof(Book));
    book_copy->copies = copies_to_remove;
    book_copy->authors = str_copy(book->authors);
    book_copy->title = str_copy(book->title);
    book_copy->year = book->year;

    return remove_book(*book_copy);
}

int add_books(BooksOwnedArray *books_array, BooksOwned *books)
{
    if (books_array == NULL || books == NULL)
        return 1;

    // insert the books into the array
    books_array->length++;
    // malloc if the array was empty
    if (books_array->length == 1)
        books_array->array = (BooksOwned *) malloc(sizeof(BooksOwned));
    // realloc if some books already were in the array
    else
        books_array->array = (BooksOwned *) realloc(books_array->array, sizeof(BooksOwned) * books_array->length);
    books_array->array[books_array->length - 1] = *books;

    return 0;
}

BooksOwnedArray *find_book_by_title_(BooksOwnedArray *books, const char *title)
{
    if (title == NULL || books == NULL)
        return NULL;

    if (str_same(title, ""))
        return NULL;

    // update edited books for book_management
    edited_books = books;

    // initialize the output and go through each of the owned books structs
    BooksOwnedArray *out = (BooksOwnedArray *) malloc(sizeof(BooksOwnedArray));
    out->length = 0;
    out->array = (BooksOwned *) malloc(sizeof(BooksOwned));
    for (int i = 0; i < books->length; ++i)
    {
        // update edited books single for book_management
        edited_books_single = &books->array[i];

        // use book_management to search for the results within edited_books_single
        BookArray *books_found = (BookArray *) malloc(sizeof(BookArray));
        *books_found = find_book_by_title(title);

        // if any books were found: add them to the output array
        if (books_found->length)
        {
            BooksOwned *books_found_owned = (BooksOwned *) malloc(sizeof(BooksOwned));
            books_found_owned->owner = books->array[i].owner;
            books_found_owned->books = books_found;

            add_books(out, books_found_owned);
        }
    }

    return out;
}

BooksOwnedArray *find_book_by_author_(BooksOwnedArray *books, const char *author)
{
    if (author == NULL || books == NULL)
        return NULL;

    if (str_same(author, ""))
        return NULL;

    // update edited books for book_management
    edited_books = books;

    // initialize the output and go through each of the owned books structs
    BooksOwnedArray *out = (BooksOwnedArray *) malloc(sizeof(BooksOwnedArray));
    out->length = 0;
    out->array = (BooksOwned *) malloc(sizeof(BooksOwned));
    for (int i = 0; i < books->length; ++i)
    {
        // update edited books single for book_management
        edited_books_single = &books->array[i];

        // use book_management to search for the results within edited_books_single
        BookArray *books_found = (BookArray *) malloc(sizeof(BookArray));
        *books_found = find_book_by_author(author);

        // if any books were found: add them to the output array
        if (books_found->length)
        {
            BooksOwned *books_found_owned = (BooksOwned *) malloc(sizeof(BooksOwned));
            books_found_owned->owner = books->array[i].owner;
            books_found_owned->books = books_found;

            add_books(out, books_found_owned);
        }
    }

    return out;
}

BooksOwnedArray *find_book_by_year_(BooksOwnedArray *books, unsigned int year)
{
    if (books == NULL)
        return NULL;

    // update edited books for book_management
    edited_books = books;

    // initialize the output and go through each of the owned books structs
    BooksOwnedArray *out = (BooksOwnedArray *) malloc(sizeof(BooksOwnedArray));
    out->length = 0;
    out->array = (BooksOwned *) malloc(sizeof(BooksOwned));
    for (int i = 0; i < books->length; ++i)
    {
        // update edited books single for book_management
        edited_books_single = &books->array[i];

        // use book_management to search for the results within edited_books_single
        BookArray *books_found = (BookArray *) malloc(sizeof(BookArray));
        *books_found = find_book_by_year(year);

        // if any books were found: add them to the output array
        if (books_found->length)
        {
            BooksOwned *books_found_owned = (BooksOwned *) malloc(sizeof(BooksOwned));
            books_found_owned->owner = books->array[i].owner;
            books_found_owned->books = books_found;

            add_books(out, books_found_owned);
        }
    }

    return out;
}

BooksOwned *find_books_by_owner(const BooksOwnedArray *books, const char *owner)
{
    if (owner == NULL || books == NULL || str_same(owner, ""))
        return NULL;

    // compares owner of given books and books in the array
    // returns the books from the array if they match
    for (int i = 0; i < books->length; ++i)
        if (str_same(books->array[i].owner, owner))
            return &books->array[i];

    // return NULL if no books matched
    return NULL;
}

char *Book_to_str(const Book *book, const int index)
{
    if (book == NULL)
        return NULL;

    // prepare string version of index and each property of the book
    char *index_str = str_combine(int_to_str(index), ".");
    char *title = str_combine("\nTitle: ", book->title);
    char *authors = str_combine("\nAuthor/s: ", book->authors);
    char *year = str_combine("\nYear of publication: ", int_to_str((int) book->year));
    char *copies = str_combine("\nCopies available: ", int_to_str((int) book->copies));

    // prepare an endline of an appropriate length (it should match the length of the longest property string)
    unsigned int endline_length = str_length(title);
    if (str_length(authors) - 1 > endline_length)   endline_length = str_length(authors) - 1;
    if (str_length(year) - 1 > endline_length)      endline_length = str_length(year) - 1;
    if (str_length(copies) - 1 > endline_length)    endline_length = str_length(copies) - 1;

    char *endline = "\n";
    for (int i = 0; i < endline_length; ++i)
        endline = str_combine(endline, "-");

    // header should also be encased in '-', so calculate how many '-' to put in front and after the book index
    // then combine the strings accordingly
    char *header = str_combine(" Book ", str_combine(index_str, " "));
    unsigned int amount = (str_length(endline) - 1 - str_length(header)) / 2;
    for (int i = 0; i < amount; i++)
        header = str_combine("-", header);
    for (int i = str_length(header); i < str_length(endline) - 1; i++)
        header = str_combine(header, "-");

    return str_combine(header, str_combine(title, str_combine(authors, str_combine(year, str_combine(copies, endline)))));
}

char *BookArray_to_str(const BookArray *books)
{
    if (books == NULL)
        return NULL;

    // converting book array to string is as easy as stating the books count and converting the books one-by-one
    char *out = "Books count: ";
    out = str_combine(out, int_to_str((int)books->length));
    out = str_combine(out, "\n\n");

    for (int i = 0; i < books->length; i++)
        out = str_combine(out, str_combine(Book_to_str(&books->array[i], i + 1), "\n\n"));

    return out;
}
