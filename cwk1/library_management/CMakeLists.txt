cmake_minimum_required(VERSION 3.10)

# set the project name
project(comp1921)

# add the executable
add_executable(main main.c book_management.h book_management.c user_manager.c user_manager.h book_manager.c book_manager.h file_utils.c file_utils.h string_utils.c string_utils.h json_utils.c json_utils.h input_utils.c input_utils.h)
