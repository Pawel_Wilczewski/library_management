#ifndef COMP1921_BOOK_MANAGER_H
#define COMP1921_BOOK_MANAGER_H

#include <stdio.h>

struct Book;
struct BookArray;
typedef struct BookArray BookArray;
typedef struct Book Book;

// BooksOwned struct is used to determine who is currently in possession of the books ("library", "joe@sample.co.uk"...)
typedef struct {
    char *owner;
    BookArray *books;
} BooksOwned;

// BooksOwned array is an array of all BooksOwned, this is the struct holding all the books in the system
// it is used when saving and storing books in files
typedef struct {
    BooksOwned *array;
    unsigned int length;
} BooksOwnedArray;

// since book_management.h can't be changed, edited_books pointer is a workaround allowing passing parameters
// to book_management functions (same with edited_user)
BooksOwnedArray *edited_books;
BooksOwned *edited_books_single;

// wrapper for store_books
// allows for passing books and custom filename
int store_books_(BooksOwnedArray *books, const char *filename);

// wrapper for load_books
// allows for passing custom filename
BooksOwnedArray *load_books_(const char *filename);

// returns 1 if the given books are the same, 0 otherwise
// in comparison, the title, author and year are used
int book_same(const Book *book1, const Book *book2);

// wrapper for add_book
// allows for passing the desired database and each of the book properties one-by-one
int add_book_(BooksOwnedArray *books_database, const char *owner,
              char *title, char *authors, unsigned int year, unsigned int copies);

// adds a set of books (BooksOwned) to the array (BooksOwnedArray)
int add_books(BooksOwnedArray *books_array, BooksOwned *books);

// wrapper for remove_book
// allows for passing the desired database and copies to remove count
// also requires an owner to be passed (to find out where to decrease books count / remove the book entirely)
int remove_book_(const BooksOwnedArray *books_database, const char *owner, const Book *book, int copies_to_remove);

// wrapper for find_book_by_title
// allows for passing books database and returns more-specific BooksOwnedArray instead of BookArray
BooksOwnedArray *find_book_by_title_(BooksOwnedArray *books, const char *title);

// wrapper for find_book_by_author
// allows for passing books database and returns more-specific BooksOwnedArray instead of BookArray
BooksOwnedArray *find_book_by_author_(BooksOwnedArray *books, const char *author);

// wrapper for find_book_by_year
// allows for passing books database and returns more-specific BooksOwnedArray instead of BookArray
BooksOwnedArray *find_book_by_year_(BooksOwnedArray *books, unsigned int year);

// returns books owned by user with the given owner string (usually LIBRARY_OWNERSHIP_NAME / user email)
BooksOwned *find_books_by_owner(const BooksOwnedArray *books, const char *owner);

// converts Book to user-friendly string
// since this function is used when listing books, index parameter comes in handy to order the books displayed
char *Book_to_str(const Book *book, int index);

// converts BookArray to user-friendly string
// displays count of books and then lists all of them one-by-one using Book_to_str
char *BookArray_to_str(const BookArray *books);

#endif //COMP1921_BOOK_MANAGER_H
