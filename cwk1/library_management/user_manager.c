#include "user_manager.h"
#include <stdlib.h>
#include "string_utils.h"
#include "json_utils.h"
#include "file_utils.h"

int store_users(const UserArray *users, const char *filename)
{
    if (users == NULL || filename == NULL)
        return 1;

    if (str_same(filename, ""))
        return -1;

    // open file, write content, close
    FILE *file = fopen(filename, "w+");
    fprintf(file, "%s", UserArray_to_json(0, users));
    fclose(file);

    return 0;
}

UserArray *load_users(const char *filename)
{
    if (filename == NULL || str_same(filename, ""))
        return NULL;

    FILE *file = fopen(filename, "r");

    // if the file didnt exist, create initialize an empty UserArray, store it and then re-open the file
    if (file == NULL)
    {
        UserArray *users = (UserArray *) malloc(sizeof(UserArray));
        users->length = 0;
        users->array = (User *) malloc(sizeof(User));

        store_users(users, filename);
        file = fopen(filename, "r");
    }

    // read file and return the output
    UserArray *out = json_to_UserArray(read_file(file));
    fclose(file);

    return out;
}

int add_user(UserArray *users, const EUserRole role, char *name, char *email, char *username, char *password)
{
    // check if safe to access any of the vars
    if (users == NULL || name == NULL || email == NULL || username == NULL || password == NULL)
        return 1;

    // check if strings are not empty
    if (str_same(name, "") || str_same(email, "") || str_same(username, "") || str_same(password, ""))
        return -1;

    // reallocate enough memory for the new user
    users->length++;
    if (users->length == 1)
        users->array = (User *) malloc(sizeof(User));
    else
        users->array = (User *) realloc(users->array, sizeof(User) * users->length);

    // create the new user and insert it to the array
    User *new_user = (User *) malloc(sizeof(User));
    new_user->role = role;
    new_user->name = name;
    new_user->email = email;
    new_user->username = username;
    new_user->password = password;
    users->array[users->length - 1] = *new_user;

    return 0;
}

User *find_user_by_email(const UserArray *users, const char *email)
{
    if (users == NULL || email == NULL)
        return NULL;

    // go through every user and compare the email property to the given argument
    // return user if the emails match
    for (int i = 0; i < users->length; ++i)
        if (str_same(users->array[i].email, email))
            return &users->array[i];

    return NULL;
}

User *find_user_by_username(const UserArray *users, const char *username)
{
    if (users == NULL || username == NULL)
        return NULL;

    // go through every user and compare the username property to the given argument
    // return user if the usernames match
    for (int i = 0; i < users->length; ++i)
        if (str_same(users->array[i].username, username))
            return &users->array[i];

    return NULL;
}

UserArray *find_user_by_role(const UserArray *users, const EUserRole role)
{
    if (users == NULL)
        return NULL;

    // create the output array that will store all the matching users
    UserArray *out = (UserArray *) malloc(sizeof(UserArray));
    out->length = 0;
    out->array = (User *) malloc(sizeof(User));

    // go through every user and compare the role property to the given argument
    // append to array if the roles match
    for (int i = 0; i < users->length; ++i)
        if (users->array[i].role == role)
        {
            out->array = (User *) realloc(out->array, ++out->length);
            out->array[out->length - 1] = users->array[i];
        }

    return out;
}

int is_username_unique(const UserArray *users, const char *username)
{
    if (users == NULL || username == NULL)
        return 0;

    // if the username overlaps with any of those in the database, return 0
    for (int i = 0; i < users->length; ++i)
        if (str_same(users->array[i].username, username))
            return 0;

    // otherwise the username is unique and return 1
    return 1;
}

int is_email_unique(const UserArray *users, const char *email)
{
    if (users == NULL || email == NULL)
        return 0;

    // if the email overlaps with any of those in the database, return 0
    for (int i = 0; i < users->length; ++i)
        if (str_same(users->array[i].email, email))
            return 0;

    // otherwise the email is unique and return 1
    return 1;
}
