#ifndef COMP1921_USER_MANAGER_H
#define COMP1921_USER_MANAGER_H

#include "stdio.h"

// enum user role used to determine whether they can add, remove books as admin, or only borrow and return
typedef enum {
    Role_User = 0,
    Role_Admin = 1
} EUserRole;

// struct used to identify and store information about user
typedef struct {
    EUserRole role;
    char *name;
    char *email;
    char *username;
    char *password;
} User;

// struct used to store and retrieve all users from the database
typedef struct {
    User *array;
    unsigned int length;
} UserArray;

// stores users from the given array to the given file
int store_users(const UserArray *users, const char *filename);

// loads users from the given file to the given array
UserArray *load_users(const char *filename);

// inserts a user to the back of the given users array
int add_user(UserArray *users, EUserRole role, char *name, char *email, char *username, char *password);

// returns user with the given email
User *find_user_by_email(const UserArray *users, const char *email);

// returns user with the given username
User *find_user_by_username(const UserArray *users, const char *username);

// returns a list of users with the given role
UserArray *find_user_by_role(const UserArray *users, EUserRole role);

// checks if the array contains the given username (1 if it doesnt, 0 otherwise)
int is_username_unique(const UserArray *users, const char *username);

// checks if the array contains the given email (1 if it doesnt, 0 otherwise)
int is_email_unique(const UserArray *users, const char *email);

#endif //COMP1921_USER_MANAGER_H
