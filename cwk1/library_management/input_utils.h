#ifndef COMP1921_INPUT_UTILS_H
#define COMP1921_INPUT_UTILS_H

#include "string_utils.h"
#include "user_manager.h"
#include "book_manager.h"

// used when allocating memory for the input strings
// should be bigger than any of the max_length's in get_string_of_length or any line in saved text files
#define MAX_STRING_INPUT_LENGTH (1000)

// used when prompting a message for some time
// like "Successfully logged in, User!"
// in microseconds
#define PROMPT_SLEEP_DURATION 2000000

// message displayed when user inputs a string not matching the given criteria
const char *ERROR_STRING_MESSAGE;

// safely returns a single character that is a substring of allowed_chars
// an example usage get_valid_char("enter a, b, 1 or 2", "ab12");
char get_valid_char(const char *prompt, const char *allowed_chars);

// safely returns text input by user up to the point he presses enter
char *get_string(const char *prompt);

// safely prompts user to enter an integer and returns the result
int get_int(const char *prompt);

// safely prompts user to enter an int in the given range and returns the result
int get_int_in_range(const char *prompt, int min, int max);

// safely prompts user to enter a string with the length between min and max and returns the result
char *get_string_of_length(const char *prompt, size_t min_length, size_t max_length);

// for registering user
char *get_name(const char *prompt, size_t min_length, size_t max_length);

// for registering user
char *get_email(const char *prompt, size_t min_length, size_t max_length);

// for registering user
char *get_username(const char *prompt, size_t min_length, size_t max_length);

// for registering user
char *get_password(const char *prompt, size_t min_length, size_t max_length);

// prompts user to enter an email that hasn't been used by any other user yet
char *get_unique_email(UserArray *existing_users, const char *prompt, size_t min_length, size_t max_length);

// prompts user to enter a username that hasn't been used by any other user yet
char *get_unique_username(UserArray *existing_users, const char *prompt, size_t min_length, size_t max_length);

// clear console screen; works both on windows and linux
void clear_screen();

// prompts a message for the amount of time specified by PROMPT_SLEEP_DURATION
void prompt_sleep(const char *message);

// prompts user to enter information for newly created user and adds it to the users array
void prompt_register_user(UserArray *users, EUserRole role);

// prompts user to log in and returns the freshly logged-in user
User *prompt_log_in_user(UserArray *allowed_users);

// prompts user to add a book to the database (admins only)
void prompt_add_book(BooksOwnedArray *books_database, const char *owner);

// prompts user to remove a book from the database (admins only)
void prompt_remove_book(BooksOwnedArray *books_database, const char *owner);

// prompts user to specify the book he wants to borrow and moves it from the library to his account
void prompt_borrow_book(BooksOwnedArray *books_database, const char *borrowing, const char *lending);

// prompts user to specify the book he wants to return and moves it from his account to the library
void prompt_return_book(BooksOwnedArray *books_database, const char *returner, const char *owner);

// prompts user to select a book under the given owner and returns the book he chose
// allows for filtering the results
Book *prompt_find_book(BooksOwnedArray *books_database, const char *owner);

#endif //COMP1921_INPUT_UTILS_H
