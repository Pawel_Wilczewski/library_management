#include "input_utils.h"
#include <stdlib.h>
#include <stdio.h>
#include "book_management.h"
#include <unistd.h>
#include <errno.h>

const char *ERROR_STRING_MESSAGE = "Invalid input! Please try again...\n";

char get_valid_char(const char *prompt, const char *allowed_chars)
{
    if (prompt == NULL || allowed_chars == NULL)
    {
        printf("get_valid_char prompt or allowed_chars are NULL!\n");
        return -1;
    }

    // keep trying to get a valid char until it indeed is valid
    char *out;
    int i = 0;
    do
    {
        if (i++ > 0)
            printf("\nInvalid choice! Please try again...\n");

        out = get_string(prompt);
    } while (out == NULL || str_same(out, "") || !str_contains(allowed_chars, out));

    return out[0];
}

char *get_string(const char *prompt)
{
    if (prompt == NULL)
    {
        printf("get_string prompt is NULL!\n");
        return NULL;
    }

    // print the prompt
    printf("%s:\n", prompt);

    // forward allocate enough memory to fit the string for sure
    char *out = (char *) malloc(sizeof(char) * MAX_STRING_INPUT_LENGTH);

    // get string until '\n'
    gets(out);

    // ensure the NULL character
    out[str_length(out)] ='\0';

    // reallocate so that memory is not wasted
    out = (char *) realloc(out, sizeof(char) * (str_length(out) + 1));

    return out;
}

int get_int(const char *prompt)
{
    // keep getting input until success
    int out;
    char *string;
    int i = 0;
    while (1)
    {
        if (i++ > 0)
            printf("%s\n", ERROR_STRING_MESSAGE);

        string = get_string(prompt);

        char *end_ptr;
        errno = 0;
        out = strtol(string, &end_ptr, 10);

        // break if no errors occurred
        if (end_ptr == string + str_length(string) && errno == 0 && !str_same(string, ""))
            break;
    }

    return out;
}

int get_int_in_range(const char *prompt, int min, int max)
{
    if (min > max)
        min = max;

    // keep getting input until success
    int out;
    int i = 0;
    do {
        if (i++ > 0)
            printf("%s\n", ERROR_STRING_MESSAGE);

        char *updated_prompt = (char *) malloc(sizeof(char) * (str_length(prompt) + 50));
        sprintf(updated_prompt, "%s (in range %i-%i)", prompt, min, max);

        out = get_int(updated_prompt);
    } while (out < min || out > max);

    return out;
}

char *get_string_of_length(const char *prompt, size_t min_length, size_t max_length)
{
    if (min_length > max_length)
        min_length = max_length;

    // keep getting input until success
    char *out;
    int i = 0;
    do {
        if (i++ > 0)
            printf("%s\n", ERROR_STRING_MESSAGE);

        char *updated_prompt = (char *) malloc(sizeof(char) * (str_length(prompt) + 50));
        sprintf(updated_prompt, "%s (%lld-%lld characters)", prompt, min_length, max_length);

        out = get_string(updated_prompt);
    } while (out == NULL || str_length(out) < min_length || str_length(out) > max_length);

    return out;
}

char *get_name(const char *prompt, size_t min_length, size_t max_length)
{
    // keep getting input until success, name shouldn't have numbers
    char *out;
    int i = 0;
    do {
        if (i++ > 0)
            printf("%s\n", "Name cannot contain numbers!");

        out = get_string_of_length(prompt, min_length, max_length);
    } while(out == NULL
            || str_contains(out, "0") || str_contains(out, "1")
            || str_contains(out, "2") || str_contains(out, "3")
            || str_contains(out, "4") || str_contains(out, "5")
            || str_contains(out, "6") || str_contains(out, "7")
            || str_contains(out, "8") || str_contains(out, "9"));

    return out;
}

char *get_email(const char *prompt, size_t min_length, size_t max_length)
{
    // keep getting input until success, email needs a '@' and shouldn't include spaces
    char *out;
    int i = 0;
    do {
        if (i++ > 0)
            printf("%s\n", "Email must contain '@' and '.' and cannot contain spaces! Please try again...");

        out = get_string_of_length(prompt, min_length, max_length);
    } while(out == NULL
            || !str_contains(out, "@") || !str_contains(out, ".") || str_contains(out, " "));

    return out;
}

char *get_username(const char *prompt, size_t min_length, size_t max_length)
{
    // keep getting input until success, username shouldn't contain spaces
    char *out;
    int i = 0;
    do {
        if (i++ > 0)
            printf("%s\n", "Username cannot contain spaces! Please try again...");

        out = get_string_of_length(prompt, min_length, max_length);
    } while(out == NULL || str_contains(out, " "));

    return out;
}

char *get_password(const char *prompt, size_t min_length, size_t max_length)
{
    // keep getting input until success, password shouldn't have spaces
    char *out;
    int i = 0;
    do {
        if (i++ > 0)
            printf("%s\n", "Password cannot contain spaces! Please try again...");

        out = get_string_of_length(prompt, min_length, max_length);
    } while(out == NULL || str_contains(out, " "));

    return out;
}

char *get_unique_email(UserArray *existing_users, const char *prompt, size_t min_length, size_t max_length)
{
    if (existing_users == NULL)
    {
        printf("get_unique_email existing_users is NULL!\n");
        return NULL;
    }

    // keep getting input until success, email should not yet be present in the database
    char *out;
    int i = 0;
    do {
        if (i++ > 0)
            printf("%s\n", "An account with this email already exists! Please try again...");

        out = get_email(prompt, min_length, max_length);
    } while (out == NULL || !is_email_unique(existing_users, out));

    return out;
}

char *get_unique_username(UserArray *existing_users, const char *prompt, size_t min_length, size_t max_length)
{
    if (existing_users == NULL)
    {
        printf("get_unique_username existing_users is NULL!\n");
        return NULL;
    }

    // keep getting input until success, username shouldn't yet be in the database
    char *out;
    int i = 0;
    do {
        if (i++ > 0)
            printf("%s\n", "An account with this username already exists! Please try again...");

        out = get_username(prompt, min_length, max_length);
    } while (out == NULL || !is_username_unique(existing_users, out));

    return out;
}

void clear_screen()
{
    // call the appropriate system command for windows or linux
#if defined(WINDOWS) || defined(WIN32) || defined(WIN64) || defined(WINNT) || defined(_WIN32) || defined(_WIN64) \
    || defined(__WIN32) || defined(__WIN64) || defined(__WIN32__) || defined(__WIN64__)
    system("cls");
#else
    system ("clear");
#endif
}

void prompt_sleep(const char *message)
{
    // print the message and wait so that user can read it
    printf("%s\n", message);
    usleep(PROMPT_SLEEP_DURATION);
}

void prompt_register_user(UserArray *users, EUserRole role)
{
    if (users == NULL)
    {
        printf("prompt_register_user users is NULL!\n");
        return;
    }

    // get all the required data and add a new user
    char *name = get_name("Enter full name", 2, 50);
    printf("\n");
    char *email = get_unique_email(users, "Enter email", 5, 50);
    printf("\n");
    char *username = get_unique_username(users, "Username", 5, 50);
    printf("\n");
    char *password = get_password("Password", 5, 50);
    printf("\n");

    add_user(users, role, name, email, username, password);

    prompt_sleep(str_combine(str_combine("Successfully registered user ", username), "!"));
}

User *prompt_log_in_user(UserArray *allowed_users)
{
    if (allowed_users == NULL)
    {
        printf("prompt_log_in_user allowed_users is NULL!\n");
        return NULL;
    }

    printf("Logging in...\n");

    // keep getting log in credentials until they match those from the database
    User *out = NULL;
    char *username;
    char *password;
    int i = 0;
    do {
        if (i++ > 0)
            printf("Invalid username or password! Please try again...\n\n");

        // use get_string_of_length for both of the inputs
        // so that program doesn't give hints what a username or password should look like
        username = get_string_of_length("Username", 1, 50);
        printf("\n");
        password = get_string_of_length("Password", 1, 50);
        printf("\n");

        out = find_user_by_username(allowed_users, username);
    } while (out == NULL || !str_same(out->password, password));

    // prompt a message on logged in
    if (out->role == Role_User)
        prompt_sleep(str_combine(str_combine("Successfully logged in as user, ", out->name), "!"));
    else if (out->role == Role_Admin)
        prompt_sleep(str_combine(str_combine("Successfully logged in as admin, ", out->name), "!"));

    return out;
}

void prompt_add_book(BooksOwnedArray *books_database, const char *owner)
{
    if (books_database == NULL)
    {
        printf("prompt_add_book books_database is NULL!\n");
        return;
    }

    // get all the required data and add a book to the database
    char *title = get_string_of_length("Book title", 1, 100);
    printf("\n");
    char *authors = get_string_of_length("Book author/authors", 1, 100);
    printf("\n");
    int year = get_int("Book year of publication");
    printf("\n");
    int copies = get_int_in_range("Book copies count", 1, 100000);
    printf("\n");
    add_book_(books_database, owner, title, authors, year, copies);

    prompt_sleep("Book added successfully!");
}

void prompt_remove_book(BooksOwnedArray *books_database, const char *owner)
{
    Book *book = prompt_find_book(books_database, owner);

    if (book == NULL)
        return;

    // get all the required data and remove the book from the database
    int copies = get_int_in_range("How many copies of book to remove", 0, (int) book->copies);
    printf("\n");
    remove_book_(books_database, owner, book, copies);

    prompt_sleep("Book removed successfully!");
}

void prompt_borrow_book(BooksOwnedArray *books_database, const char *borrowing, const char *lending)
{
    Book *book = prompt_find_book(books_database, lending);

    if (book == NULL || book->copies == 0)
    {
        prompt_sleep("Book was not found!");
        return;
    }
    BooksOwned* borrowed_books = find_books_by_owner(books_database, borrowing);

    if (borrowed_books != NULL && borrowed_books->books != NULL && borrowed_books->books->length > 0)
        for (int i = 0; i < borrowed_books->books->length; ++i)
            if (book_same(&borrowed_books->books->array[i], book))
            {
                prompt_sleep("You have already borrowed this book!");
                return;
            }

    // get all the required data and add a borrow the book (remove from lending account and add to borrowing account)
    printf("\n");
    remove_book_(books_database, lending, book, 1);
    add_book_(books_database, borrowing, book->title, book->authors, book->year, 1);

    prompt_sleep("Book borrowed successfully!");
}

void prompt_return_book(BooksOwnedArray *books_database, const char *returner, const char *owner)
{
    Book *book = prompt_find_book(books_database, returner);

    if (book == NULL)
        return;

    // get all the required data and add a return the book (remove from returner's account and add to owner's account)
    printf("\n");
    remove_book_(books_database, returner, book, 1);
    add_book_(books_database, owner, book->title, book->authors, book->year, 1);

    prompt_sleep("Book returned successfully!");
}

Book *prompt_find_book(BooksOwnedArray *books_database, const char *owner)
{
    if (books_database == NULL)
    {
        printf("prompt_find_book books_database is NULL!\n");
        return NULL;
    }

    printf("Searching for books...\n");

    // if there are no books under the given owner name, prompt that is the case
    BooksOwned *books_owned = find_books_by_owner(books_database, owner);
    if (books_owned == NULL || books_owned->books == NULL || books_owned->books->length == 0)
    {
        prompt_sleep("No books found!");
        return NULL;
    }

    // print out the menu
    printf("0. Do not filter...\n");
    printf("1. Filter by title...\n");
    printf("2. Filter by author...\n");
    printf("3. Filter by year...\n");
    printf("4. Cancel.\n");

    char choice = get_valid_char("Choice", "01234");

    // filter the results accordingly
    BooksOwned *books_found = NULL;
    if (choice == '0')
    {
        books_found = find_books_by_owner(books_database, owner);
    }
    else if (choice == '1')
    {
        char *lf_title = get_string_of_length("Book title", 1, 100);
        books_found = find_books_by_owner(find_book_by_title_(books_database, lf_title), owner);
        free(lf_title);
    }
    else if (choice == '2')
    {
        char *lf_author = get_string_of_length("Book author", 1, 100);
        books_found = find_books_by_owner(find_book_by_author_(books_database, lf_author), owner);
        free(lf_author);
    }
    else if (choice == '3')
    {
        int lf_year = get_int("Book year of publication");
        books_found = find_books_by_owner(find_book_by_year_(books_database, lf_year), owner);
    }
    else if (choice == '4')
        return NULL;

    // return if no results found
    if (books_found == NULL || books_found->books == NULL)
    {
        prompt_sleep("No books found!");
        return NULL;
    }

    // create an interface allowing user to choose a book within those that were found
    BookArray* books = books_found->books;
    int i = 0;
    while (1)
    {
        clear_screen();

        // display all the books available
        printf("All books found:\n%s\n\n", BookArray_to_str(books));

        // display possible options
        // accordingly to the index of the currently selected book (select this, next, previous and cancel)
        char *allowed_chars = "03";
        printf("Current book selected:\n%s\n\n", Book_to_str(&books->array[i], i + 1));
        printf("%s\n", "0. Select this book.");

        printf("%s\n", i > 0 ? "1. Previous book..." : "1. ----------------");
        allowed_chars = str_combine(allowed_chars, i > 0 ? "1" : "");

        printf("%s\n", i < books->length - 1 ? "2. Next book..." : "2. ------------");
        allowed_chars = str_combine(allowed_chars, i < books->length - 1 ? "2" : "");

        printf("%s\n", "3. Cancel book selection.");

        choice = get_valid_char("Choice", allowed_chars);

        // navigate through the book array accordingly to user's input
        if (choice == '0')
            return &books->array[i];
        else if (choice == '1')
            i--;
        else if (choice == '2')
            i++;
        else if (choice == '3')
            break;
    }
    return NULL;
}
