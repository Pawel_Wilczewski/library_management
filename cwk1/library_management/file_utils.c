#include "file_utils.h"
#include <stdio.h>
#include "input_utils.h"

char * read_file_filename(const char *filename)
{
    // open file, read it and return the result
    FILE *file = fopen(filename, "r");
    return read_file(file);
}

char * read_file(FILE *file)
{
    // read the file line-by-line and return the result
    char *out = "";
    char buffer[MAX_STRING_INPUT_LENGTH];
    while(fgets(buffer, MAX_STRING_INPUT_LENGTH, file))
        out = str_combine(out, buffer);

    return out;
}
