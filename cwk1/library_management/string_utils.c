#include "string_utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int str_same(const char *str1, const char *str2)
{
    return !strcmp(str_copy(str1), str_copy(str2));
}

size_t str_length(const char *string)
{
    if (string == NULL || str_same(string, ""))
        return 0;

    return strlen(string);
}

int str_find(const char *string, const char *pattern, const size_t start_index)
{
    if (!str_contains(string, pattern))
        return -1;

    if (string == NULL || pattern == NULL || start_index >= strlen(string) - strlen(pattern) + 1)
        return -1;

    char *found = strstr(string + start_index, pattern);

    if (found == NULL)
        return -1;

    return (int)((found -  string) / sizeof(char)); // NOLINT(bugprone-sizeof-expression)
}

int str_find_r(const char *string, const char *pattern, int stop_index)
{
    if (!str_contains(string, pattern))
        return -1;

    if (stop_index < 0)
        stop_index = (int) strlen(string) - 1;

    if (string == NULL || pattern == NULL || str_same(string, "") || str_same(pattern, "")
        || stop_index < strlen(pattern))
        return -1;

    int reversed_position = str_find(str_reverse(string), str_reverse(pattern), strlen(string) - stop_index - 1);

    if (reversed_position == -1)
        return -1;

    return (int) (strlen(string) - reversed_position - strlen(pattern));
}

int str_count(const char *string, const char *substring)
{
    if (string == NULL || substring == NULL)
        return 0;

    if (!strlen(substring))
        return -1;

    size_t len = strlen(string);
    size_t sublen = strlen(substring);

    int i = 0, j, subcount, count = 0;
    while (i < len)
    {
        j = 0;
        subcount = 0;
        while (string[i] == substring[j])
        {
            subcount++;
            i++;
            j++;
        }
        if (subcount >= sublen)
            count++;
        else
            i++;
    }
    return count;
}

int str_contains(const char *string, const char *substring)
{
    return strstr(string, substring) != NULL;
}

char *str_copy(const char *string)
{
    return str_combine("", string);
}

char *str_combine(const char *str1, const char *str2)
{
    if (str1 == NULL || str2 == NULL)
        return NULL;

    size_t size = strlen(str1) + strlen(str2) + 1; // +1 for '\0'

    char *out = malloc(sizeof(char) * size);
    strcpy(out, str1);
    strcpy(out + strlen(str1), str2);

    // ensure the NULL char
    out[size - 1] = '\0';

    return out;
}

char *str_lower(const char *string)
{
    if (string == NULL)
        return NULL;

    char *out = "";
    for (int i = 0; i < str_length(string); ++i)
    {
        char lower[2];
        lower[0] = (char) tolower(string[i]);
        lower[1] = '\0';
        out = str_combine(out, lower);
    }

    return out;
}

char *str_upper(const char *string)
{
    if (string == NULL)
        return NULL;

    char *out = "";
    for (int i = 0; i < str_length(string); ++i)
    {
        char upper[2];
        upper[0] = (char) toupper(string[i]);
        upper[1] = '\0';
        out = str_combine(out, upper);
    }

    return out;
}

char *str_reverse(const char *string)
{
    if (string == NULL)
        return NULL;

    size_t len = strlen(string);
    char *out = (char *) malloc(sizeof(char) * (len + 1));
    out[len] = '\0';

    for (int i = 0; i < len; ++i)
        out[i] = string[len - i - 1];

    return out;
}

char *str_replace(char *string, const char *to_replace, const char *replace_with, int n)
{
    // skip if invalid to_replace string
    if (str_same(to_replace, ""))
        return string;

    // if infinite - adjust n
    if (n < 1 || n > str_count(string, to_replace))
        n = str_count(string, to_replace);

    // initialize the output and a temporary buffer that will help "cutting" and replacing the substrings
    char *out = "";
    char *todo = str_copy(string);

    // go through the string and add parts before the replacement, then the appropriate replacement
    int i = 0;
    char *before;
    while ((before = str_substr_before(todo, to_replace, 0)) != NULL && i < n)
    {
        out = str_combine(out, before);
        out = str_combine(out, replace_with);
        todo = str_substr_after(todo, to_replace, 0);

        ++i;
    }
    // finally add the last part after the last replacement
    out = str_combine(out, todo);

    return out;
}

char *str_replace_r(char *string, const char *to_replace, const char *replace_with, int n)
{
    return str_reverse(str_replace(str_reverse(string), str_reverse(to_replace), str_reverse(replace_with), n));
}

char *str_replace_between(char *string, const char *pattern1, const char *pattern2, const char *replace_with,
                          int include1, int include2, int n)
{
    int count1 = str_count(string, pattern1);
    int count2 = str_count(string, pattern2);
    int max_n = count1 < count2 ? count1 : count2;

    if (n < 1 || n > max_n)
        n = max_n;

    char *out = "";
    char *todo = str_copy(string);
    for (int i = 0; i < n; i++)
    {
        size_t replacement_start = str_find(todo, pattern1, 0);
        size_t replacement_end = str_find(todo, pattern2, replacement_start) + 0;

        out = str_combine(out, str_substr(todo, 0, replacement_start));
        if (!include1)
            out = str_combine(out, pattern1);
        out = str_combine(out, replace_with);
        if (!include2)
            out = str_combine(out, pattern2);

        todo = str_substr(todo, replacement_end + strlen(pattern2), -1);
    }
    out = str_combine(out, todo);

    return out;
}

char *str_substr(const char *string, size_t start, size_t stop)
{
    if (string == NULL)
        return NULL;

    if (stop < start || stop > strlen(string))
        stop = strlen(string);

    if (start > strlen(string) || start < 0)
        return NULL;
    else if (start == strlen(string))
        return "";

    size_t size = stop - start + 1;
    char *out = (char *) malloc(sizeof(char) * size);

    strncpy(out, string + (sizeof(char) * start), sizeof(char) * (stop - start));
    out[size - 1] = '\0';

    return out;
}

char *str_substr_after(const char *string, const char *pattern, int inclusive)
{
    if (string == NULL || pattern == NULL)
        return NULL;

    if (strlen(pattern) <= 0)
        return str_copy(string);

    char *out = str_substr(string, str_find(string, pattern, 0) + strlen(pattern), -1);

    if (inclusive)
        out = str_combine(pattern, out);

    return out;
}

char *str_substr_before(const char *string, const char *pattern, int inclusive)
{
    if (string == NULL || pattern == NULL)
        return NULL;

    if (strlen(pattern) <= 0)
        return str_copy(string);

    char *out = str_substr(string, 0, str_find(string, pattern, 0));

    if (inclusive)
        out = str_combine(out, pattern);

    return out;
}

StringArray *str_split(const char *string, const char *delimiter)
{
    if (string == NULL || delimiter == NULL || str_same(delimiter, ""))
        return NULL;

    if (str_same(string, ""))
    {
        StringArray *out = (StringArray *) malloc(sizeof(StringArray));
        out->length = 0;
        out->array = (char **) malloc(sizeof(char *));

        return out;
    }

    char *todo = str_copy(string);

    // remove trailing delimiter if present
    if (str_same(str_substr(todo, strlen(todo) - strlen(delimiter), -1), delimiter))
        todo = str_replace_r(todo, delimiter, "", 1);

    char **out = (char **) malloc(sizeof(char *) * (str_count(todo, delimiter) + 1));

    int i = 0;
    while (str_contains(todo, delimiter))
    {
        out[i] = str_substr_before(todo, delimiter, 0);
        todo = str_substr_after(todo, delimiter, 0);
        i++;
    }
    out[i] = todo;

    StringArray *result = (StringArray *) malloc(sizeof(StringArray));
    result->array = out;
    result->length = i + 1;

    return result;
}

char * int_to_str(const int x)
{
    // find out the size using NULL pointer first
    int len = snprintf(NULL, 0, "%d", x) + 1;

    // create a string using the found out length
    char* str = (char *) malloc(sizeof(char) * len);
    snprintf(str, len, "%d", x);

    // ensure the null char
    str[len - 1] = '\0';

    return str;
}

int str_to_int(const char *string)
{
    return atoi(string);
}
