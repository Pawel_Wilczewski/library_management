#include "book_management.h"
#include "book_manager.h"
#include "json_utils.h"
#include <stdlib.h>
#include "string_utils.h"
#include "file_utils.h"

int store_books(FILE *file)
{
    if (file == NULL || edited_books == NULL)
        return 1;

    // user json parser to convert books from array to text
    fprintf(file, "%s", BooksOwnedArray_to_json(0, edited_books));

    return 0;
}

int load_books(FILE *file)
{
    if (file == NULL)
        return 1;

    // user json parser to convert books from text back to array
    edited_books = json_to_BooksOwnedArray(read_file(file));

    return 0;
}

int add_book(struct Book book)
{
    if (edited_books_single == NULL)
        return 1;

    // check if the book being added is already in the array
    Book *to_add = NULL;
    for (int i = 0; i < edited_books_single->books->length; ++i)
    {
        Book *current_book = &edited_books_single->books->array[i];
        if (book_same(current_book, &book))
            to_add = current_book;
    }

    // if the book is not yet in the array, resize the array and insert the given book into it
    if (to_add == NULL)
    {
        edited_books_single->books->length++;
        if (edited_books_single->books->length == 1)
            edited_books_single->books->array = (Book *) malloc(sizeof(Book));
        else
            edited_books_single->books->array = (Book *) realloc(edited_books_single->books->array,
                                                                 sizeof(Book) * edited_books_single->books->length);
        edited_books_single->books->array[edited_books_single->books->length - 1] = book;
    }
    // if the books is in the array, just increase the copies of it
    else
        to_add->copies += book.copies;

    return 0;
}

int remove_book(struct Book book)
{
    if (edited_books_single == NULL)
        return 1;

    // check if the array contains book to remove
    Book *to_remove = NULL;
    for (int i = 0; i < edited_books_single->books->length; ++i)
    {
        Book *current_book = &edited_books_single->books->array[i];
        if (book_same(current_book, &book))
        {
            to_remove = current_book;
            break;
        }
    }

    // return code if the book is not in the array
    if (to_remove == NULL)
        return 2;

    // remove the book from the array entirely if edited_copies >= copies
    if (book.copies >= to_remove->copies)
    {
        BookArray *new_arr = (BookArray *) malloc(sizeof(BookArray));
        new_arr->length = edited_books_single->books->length - 1;
        new_arr->array = (Book *) malloc(sizeof(Book) * new_arr->length);

        // recreate the array with the book to remove skipped
        int removed = 0;
        for (int i = 0; i < edited_books_single->books->length; ++i)
        {
            Book *current_book = &edited_books_single->books->array[i];

            if (!book_same(current_book, to_remove))
                new_arr->array[i - removed] = *current_book;
            else
                removed = 1;
        }

        edited_books_single->books = new_arr;
    }
    // if the books is in the array, just decrease the copies of it
    else
        to_remove->copies -= book.copies;

    return 0;
}

struct BookArray find_book_by_title(const char *title)
{
    // return an invalid result if the provided books are NULL
    if (edited_books_single == NULL)
    {
        BookArray invalid;
        invalid.array = NULL;
        invalid.length = 0;
        return invalid;
    }

    // initialize the array of out books, go through each book and compare it...
    // if applicable: add the book to the out array
    BookArray *out = (BookArray *) malloc(sizeof(BookArray));
    out->length = 0;
    out->array = (Book *) malloc(sizeof(Book));
    for (int i = 0; i < edited_books_single->books->length; i++)
    {
        Book *current_book = &edited_books_single->books->array[i];
        printf("Checking %s with %s\n", str_lower(current_book->title), str_lower(title));
        if (str_contains(str_lower(current_book->title), str_lower(title)))
        {
            out->length++;
            out->array = (Book *) realloc(out->array, sizeof(Book) * out->length);
            out->array[out->length - 1] = *current_book;
        }
    }

    return *out;
}

struct BookArray find_book_by_author(const char *author)
{
    // return an invalid result if the provided books are NULL
    if (edited_books_single == NULL)
    {
        BookArray invalid;
        invalid.array = NULL;
        invalid.length = 0;
        return invalid;
    }

    // initialize the array of out books, go through each book and compare it...
    // if applicable: add the book to the out array
    BookArray *out = (BookArray *) malloc(sizeof(BookArray));
    out->length = 0;
    out->array = (Book *) malloc(sizeof(Book));
    for (int i = 0; i < edited_books_single->books->length; i++)
    {
        Book *current_book = &edited_books_single->books->array[i];
        if (str_contains(str_lower(current_book->authors), str_lower(author)))
        {
            out->length++;
            out->array = (Book *) realloc(out->array, sizeof(Book) * out->length);
            out->array[out->length - 1] = *current_book;
        }
    }

    return *out;
}

struct BookArray find_book_by_year(unsigned int year)
{
    // return an invalid result if the provided books are NULL
    if (edited_books_single == NULL)
    {
        BookArray invalid;
        invalid.array = NULL;
        invalid.length = 0;
        return invalid;
    }

    // initialize the array of out books, go through each book and compare it...
    // if applicable: add to the out array
    BookArray *out = (BookArray *) malloc(sizeof(BookArray));
    out->length = 0;
    out->array = (Book *) malloc(sizeof(Book));
    for (int i = 0; i < edited_books_single->books->length; i++)
    {
        Book *current_book = &edited_books_single->books->array[i];
        if (current_book->year == year)
        {
            out->length++;
            out->array = (Book *) realloc(out->array, sizeof(Book) * out->length);
            out->array[out->length - 1] = *current_book;
        }
    }

    return *out;
}
